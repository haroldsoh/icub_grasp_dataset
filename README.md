# README #

Due to Bitbucket sunsetting their mercurial repos, the dataset has been moved to the CLeAR github site [https://github.com/clear-nus/icub_grasp_dataset](https://github.com/clear-nus/icub_grasp_dataset)

More tactile learning stuff (papers and code) [at https://clear-nus.github.io/tactile](https://clear-nus.github.io/tactile)